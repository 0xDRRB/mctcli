#ifndef OUTPUT_INC
#define OUTPUT_INC

void printhelp(char *binname);
void printprogressK(Keymap *myKM, int nbrsect, int secnum, int nbrkeys, int keynum);
void printprogressB(Keymap *myKM, int nbrblk, int blknum);
void printmapping(Keymap *myKM, int nbrsect);
int printmfdata(int nbrsect, unsigned char *src);
void printkey();

#endif

