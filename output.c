#include <nfc/nfc.h>
#include <freefare.h>

#include "main.h"
#include "colors.h"
#include "output.h"

extern int optprogress;
extern MifareClassicKey *keylist;
extern int nbrkeys;

void
printhelp(char *binname)
{
	printf("Mifare Classic Tool CLI v0.0.3\n");
	printf("Copyright (c) 2019 - Denis Bodor\n\n");
	printf("Usage : %s [OPTIONS]\n", binname);
	printf(" -k FILE         read keys from FILE\n");
	printf(" -l              display keylist\n");
	printf(" -L              list available readers/devices\n");
	printf(" -D connstring   use this device (default: use the first available device)\n");
	printf(" -m              just map and display keymap\n");
	printf(" -r              read tag (implies -m)\n");
	printf(" -d              display tag data (implies -r and -m)\n");
	printf(" -p              display fancy progress bar\n");
	printf(" -f FILE         write data in FILE (extension set the format : bin, mct, eml)\n");
	printf(" -y              overwrite data FILE if exists\n");
	printf(" -h              show this help\n");
}

// display progress bar for keymapping
void
printprogressK(Keymap *myKM, int nbrsect, int secnum, int nbrkeys, int keynum)
{
	int wide;
	int i;
	char linebuff[LINEBUFSZ];

	printf("Mapping... [");
	if (nbrsect == 16) {
		snprintf(linebuff, LINEBUFSZ-1, " Sector: %2d/%-2d   Key: %4d/%-4d ", secnum+1, nbrsect, keynum+1, nbrkeys);  // 16 sect = 32 char
		wide = 2;
	} else {
		snprintf(linebuff, LINEBUFSZ-1, "   Sector: %2d/%-2d       Key: %4d/%-4d   ", secnum+1, nbrsect, keynum+1, nbrkeys);  // 40 sect = 40 char
		wide = 1;
	}
	for (i = 0; linebuff[i] != 0; i++) {
		if (i < secnum*wide+wide) {  // color previous sectors
			if (myKM[i/wide].keyA && myKM[i/wide].keyB) printf(WGREEN);  // all keys found for this sector
			else if (myKM[i/wide].keyA || myKM[i/wide].keyB) printf(WYELLOW);  // one key missing
			else printf(WRED); // no key
		}
		printf("%c", linebuff[i]);
		printf(RESET);
	}
	printf(RESET "]\r");
	fflush(stdout);
}

// display progress bar for blocks reading
void
printprogressB(Keymap *myKM, int nbrblk, int blknum)
{
	int div = 1;
	int i;
	char linebuff[LINEBUFSZ];

	printf("Reading... [");
	snprintf(linebuff, LINEBUFSZ-1, "                         block %3d/%-3d                          ", blknum+1, nbrblk);

	if (nbrblk == 256)
		div = 4;

	for (i = 0; linebuff[i] != 0; i++) {
		if (i < blknum/div+1) {  // color previous blocks
			if (myKM[mifare_classic_block_sector(i*div)].readB & (1 << (i*div-mifare_classic_sector_first_block(mifare_classic_block_sector(i*div))))
					&& myKM[mifare_classic_block_sector(i*div)].keyB != NULL)
				printf(WGREEN);
			else if (myKM[mifare_classic_block_sector(i*div)].readA & (1 << (i*div-mifare_classic_sector_first_block(mifare_classic_block_sector(i*div))))
					&& myKM[mifare_classic_block_sector(i*div)].keyA != NULL)
				printf(WGREEN);
			else {
				if (div == 1)
					printf(WRED);
				else
					printf(WYELLOW);
			}
		}
		printf("%c", linebuff[i]);
		printf(RESET);
	}

	printf(RESET "]\r");
	fflush(stdout);
}

void
printmapping(Keymap *myKM, int nbrsect)
{
	int countkeys = 0;
	int i;

	printf("        key A         key B         ReadA    ReadB\n");
	for (i = 0; i < nbrsect; i++) {
		MifareClassicKey *tmpkeyA = myKM[i].keyA;
		MifareClassicKey *tmpkeyB = myKM[i].keyB;

		printf("%02d:  ", i);
		if (tmpkeyA != NULL)
			printf("%02x%02x%02x%02x%02x%02x", (*tmpkeyA)[0],(*tmpkeyA)[1],(*tmpkeyA)[2],(*tmpkeyA)[3],(*tmpkeyA)[4],(*tmpkeyA)[5]);
		else
			printf("------------");
		if (tmpkeyB != NULL)
			printf("  %02x%02x%02x%02x%02x%02x", (*tmpkeyB)[0],(*tmpkeyB)[1],(*tmpkeyB)[2],(*tmpkeyB)[3],(*tmpkeyB)[4],(*tmpkeyB)[5]);
		else
			printf("  ------------");
		printf("     %04X", myKM[i].readA);
		printf("     %04X", myKM[i].readB);
		printf("\n");
	}

	for (i = 0; i < nbrsect; i++) {
		countkeys += myKM[i].keyA == NULL ? 0 : 1;
		countkeys += myKM[i].keyB == NULL ? 0 : 1;
	}
	if (countkeys == nbrsect*2)
		printf("Found all keys\n");
	else
		printf(BOLDRED"Keymap incomplete: "RESET"%d/%d\n", countkeys, (nbrsect*2));
}

int
printmfdata(int nbrsect, unsigned char *src)
{
	int i, j, k;
	for (i = 0; i < nbrsect; i++) {
		printf("+Sector: %d\n", i);
		for (k = mifare_classic_sector_first_block(i); k <= mifare_classic_sector_last_block(i); k++) {
			if (k == 0) printf(MAGENTA); // sector 0 is special
			for (j = 0; j < 16; j++) {
				if (k == mifare_classic_sector_last_block(i)) { // last sector of a block is special
					if (j == 0) printf(BOLDGREEN);
					if (j == 6) printf(YELLOW);
					if (j == 10) printf(GREEN);
				}
				printf("%02X", src[(k*16)+j]);
			}
			printf(RESET "\n");
		}
	}
	return 0;
}

void
printkey()
{
	printf("Key list:\n");
	for (int i = 0; i < nbrkeys; i++) {
		printf("%5d: %02X %02X %02X %02X %02X %02X\n", i, keylist[i][0], keylist[i][1], keylist[i][2], keylist[i][3], keylist[i][4], keylist[i][5]);
	}
}

