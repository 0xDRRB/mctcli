#ifndef FILEOPS_INC
#define FILEOPS_INC

int savemfdata_txt(int nbrsect, unsigned char *src, FILE *fp, bool mct);
int savemfdata(int nbrsect, unsigned char *src, char *filename);
int loadkeys(const char *filename);

#endif

