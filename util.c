#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>

#include "util.h"

int
bcd2bin(uint8_t val) {
	return ( (((val & 0xf0) >> 4)*10) + (val & 0x0f) );
}

// check for suffix in string
bool
strendswith(const char *s,  const char *suffix)
{
	size_t ls = strlen(s);
	size_t lsuffix = strlen(suffix);
	if (ls >= lsuffix) {
		return strncmp(suffix, s + (ls - lsuffix), lsuffix) == 0;
	}
	return false;
}

