#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <sys/stat.h>
#include <nfc/nfc.h>
#include <freefare.h>

#include "fileops.h"
#include "util.h"

extern int optoverwrite;
extern MifareClassicKey *keylist;

int
savemfdata_txt(int nbrsect, unsigned char *src, FILE *fp, bool mct)
{
	int i, j, k;
	for (i = 0; i < nbrsect; i++) {
		if(mct)
			if (fprintf(fp, "+Sector: %d\n", i) == -1)
				return -1;
		for (k = mifare_classic_sector_first_block(i); k <= mifare_classic_sector_last_block(i); k++) {
			for (j = 0; j < 16; j++) {
				if (fprintf(fp, "%02X", src[(k*16)+j]) == -1)
					return -1;
			}
			if (fprintf(fp, "\n") == -1)
				return -1;
		}
	}
	return 0;
}

int
savemfdata(int nbrsect, unsigned char *src, char *filename)
{
	struct stat st;
	FILE *fp;
	int nbrblck = 64;

	if (nbrsect == 40)
		nbrblck = 256;

	// stat if !
	if (stat(filename, &st) == 0 && !optoverwrite) {
		fprintf(stderr, "Destination file exists. Use -y to overwrite!\n");
		return -1;
	}

	fp = fopen(filename, "wb");
	if (fp == NULL) {
		fprintf(stderr, "Error opening file: %s\n", strerror(errno));
		return -1;
	}

	if (strendswith(filename, ".bin")) {
		printf("Writing %s in binary format...\n", filename);
		if(fwrite(src, sizeof(uint8_t), nbrblck*16, fp) != nbrblck*16) {
			fprintf(stderr, "Error writing file: %s\n", strerror(errno));
			return -1;
		}
	} else if (strendswith(filename, ".mct")) {
		printf("Writing %s in MCT (Mifare Classic Tool Android App) format...\n", filename);
		if (savemfdata_txt(nbrsect, src, fp, true) == -1) {
			fprintf(stderr, "Error writing file! Abord!\n");
			fclose(fp);
			return -1;
		}
	} else if (strendswith(filename, ".eml")) {
		printf("Writing %s in EML (Proxmark3) format...\n", filename);
		if (savemfdata_txt(nbrsect, src, fp, false) == -1) {
			fprintf(stderr, "Error writing file! Abord!\n");
			fclose(fp);
			return -1;
		}
	} else {
		fclose(fp);
		fprintf(stderr, "Unknown file format!\n");
		return -1;
	}

	fclose(fp);
	return 0;
}



int
loadkeys(const char *filename)
{
	FILE *fp;
	char *strline = NULL;
	int line = 0;
	size_t len = 0;
	ssize_t read;
	int count = 0;

	MifareClassicKey tmpkey;

	fp = fopen(filename, "rt");
	if (fp == NULL)
		return(0);

	while ((read = getline(&strline, &len, fp)) != -1) {
		// ignore empty line or starting with '#'
		if (strline[0] == '#' || strline[0] == '\n') continue;
		if (sscanf(strline, "%2hhx%2hhx%2hhx%2hhx%2hhx%2hhx", &tmpkey[0], &tmpkey[1], &tmpkey[2], &tmpkey[3], &tmpkey[4], &tmpkey[5]) == 6) {
			if ((keylist = (MifareClassicKey *)realloc(keylist, (count+1)*sizeof(MifareClassicKey))) == NULL) {
				fprintf(stderr, "malloc list error: %s\n", strerror(errno));
			}
			memcpy(keylist[count], &tmpkey, sizeof(MifareClassicKey));
			count++;
		} else {
			fprintf(stderr, "Bad line syntax at line %d\n", line);
		}
		line++;
	}

	fclose(fp);
	if (strline)
		free(strline);

	if (count)
		printf("%d key(s) loaded from %s\n", count, filename);

	return count;
}
