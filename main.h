#ifndef MAIN_INC
#define MAIN_INC

#define LINEBUFSZ 80

#define KEYFILENAME "mctcli_keys.dic"

#ifndef SYSKEYFILE
#define SYSKEYFILE "/usr/share/mctcli/" KEYFILENAME
#endif

typedef struct {
    MifareClassicKey *keyA; // pointer to key A in keylist
    MifareClassicKey *keyB; // pointer to key B in keylist
    uint16_t readA;         // bitmap of sectors read by this key A in the bloc
    uint16_t readB;         // bitmap of sectors read by this key B in the bloc
    uint16_t writeA;        // TODO
    uint16_t writeB;        // TODO
} Keymap;

#endif

